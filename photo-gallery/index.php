<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Photo Gallery</title>
</head>
<body>

<div class="photo-gallery photo-gallery-1--columns photo-gallery-1--margin photo-gallery-1--scale-inside photo-gallery-1--scale-outside photo-gallery-1--shadow-sides">
    <div class="d-flex flex-wrap flex-column flex-sm-row justify-content-start">
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/autonovum.jpg" class="bg">
            </div>

            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/autonovum-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/rmotors.jpg" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/rmotors-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/poldek.jpg" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/poldek-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/befree.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/befree-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/sailstore.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/sailstore-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/skaut.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/skaut-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/kdrc.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/kdrc-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/medhair.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/medhair-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/yoodesign.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/yoodesign-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/daramed.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/daramed-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/lexanddesign.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/lexanddesign-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/niegrzeszne.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/niegrzeszne-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/expert.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/expert-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/adwokatks.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/adwokatks-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
        <div class="work-container work-container--inside-scale">
            <div class="work-container__background">
                <img src="./img/klienci/bg/innovation.jpg" alt="w3" class="bg">
            </div>
            <div class="layer-1 d-flex justify-content-center align-items-center">
                <div class="layer-1__overlay layer-1__overlay--overlay layer-1__overlay--logo d-flex justify-content-center align-items-center">
                    <img src="./img/klienci/w/innovation-logo.png" alt="Logo Autonovum" class="logo">
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>