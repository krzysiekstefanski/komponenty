const path = require('path');

module.exports = {
    entry: ['./build/js/app.js'],
    output: {
        path: path.resolve(__dirname, './app'),
        //path: '/user/mac/Sites/komponenty/build',
        filename: 'bundle.js'
    },

    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }]
    }
}